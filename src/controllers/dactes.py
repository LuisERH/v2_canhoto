import base64
import json
import os
from datetime import datetime
from functools import reduce
from time import time
from pathlib import Path
import boto3
import cv2
import numpy as np
from PIL import Image
from flask import current_app
from flask import jsonify, request, make_response
from flask_restplus import Resource, reqparse
from spacy import load
import shutil
from models.dactes import dacte
from server.instance import server
from utils import ocr, processing as pro, text_extraction as tex, plots
from utils import spread_sheet
import requests
from base64 import b64encode

app, api = server.app, server.api

with open(r"./credentials/aws.json") as json_file:
    aws_creds = json.load(json_file)


def build_yolo_net(yolo_path):
    weights_path = os.path.sep.join([yolo_path, 'yolov4_custom_best.weights'])
    config_path = os.path.sep.join([yolo_path, 'yolov4_custom.cfg'])
    names_path = os.path.sep.join([yolo_path, 'obj.names'])
    names = open(names_path, 'rt').read().split("\n")

    loaded_uolo_opencv = cv2.dnn_DetectionModel(config_path, weights_path)
    loaded_uolo_opencv.setInputSize((608, 608))
    loaded_uolo_opencv.setInputScale(1.0 / 255)
    loaded_uolo_opencv.setInputSwapRB(True)

    return loaded_uolo_opencv


# net = build_yolo_net('./AI_models/yolo_model')
nlp = load("AI_models/ner_model")

COR_POLIGONO_PALAVRA = (0, 0, 255)
COR_POLIGONO_CAMPO = (255, 125, 125)
COMPRIMENTO_PADRAO_IMAGEM = 1500
MIN_QUALIDADE_OCR = 68
ERRO_BAIXA_QUALIDADE = {"ERROR": "IMAGEM POSSUI BAIXA QUALIDADE"}
ERRO_LEITURA = {"ERROR": "FALHA AO FAZER LEITURA"}
ERRO_CONTEUDO_CANHOTO = {"ERROR": "IMAGEM DEVE CONTER APENAS O CONHOTO"}
ERRO_CANHOTO_PARCIAL = {"ERROR": "LEITURA PARCIAL DO CANHOTO"}

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"./credentials/GoogleVIsion.json"

s3_client = boto3.client('s3',
                         aws_access_key_id=aws_creds['aws_access_key_id'],
                         aws_secret_access_key=aws_creds['aws_secret_access_key'])


def name_obj(bucket_name, fname):
    try:
        # response = s3_client.list_objects(Bucket=bucket_name)
        momento_requisicao = datetime.now()
        momento_requisicao = str(momento_requisicao).replace(' ', '').replace('-', '').replace(':', '').replace('.', '')
        fname = os.path.basename(fname).split('.')[0]
        last = momento_requisicao + '_' + fname[:10] + '.jpeg'
        last = ''.join(last.split())
        return last
    except Exception as e:
        # raise
        # print(e)
        return "img.jpeg"


@api.route('/dacte')
class BookList(Resource):
    # @api.expect(dacte, validate=True)
    # @api.marshal_with(dacte, mask=False)
    @api.doc(parser=dacte)
    def post(self):
        try:
            inicio = time()
            parser = reqparse.RequestParser()
            if 'image_url' in request.values.to_dict().keys():
                url = request.values["image_url"]
                path = os.path.join(current_app.config['UPLOAD_FOLDER'], 'jpeg.png')
                with open(path, 'wb') as out_file:
                    shutil.copyfileobj(requests.get(url, stream=True).raw, out_file)
            else:
                file = request.files['image']
                path = os.path.join(current_app.config['UPLOAD_FOLDER'], file.filename)
                file.save(path)

            try:
                image_ori = pro.rotate(cv2.imread(path))
            except Exception as e:
                # raise
                print(e)
                image_ori = pro.rotate(np.asarray(Image.open(path)))
            cropped, response = ocr.align_image(image_ori, COMPRIMENTO_PADRAO_IMAGEM, check=False)
            if response == 400: return make_response(jsonify(cropped), response)
            inicio = time()
            try:
                if os.path.isfile(path):
                    [f.unlink() for f in Path("./files").glob("*") if f.is_file()]
                    with open('./files/files.txt', 'w') as f: f.write('')

                poligonos = pro.find_polygons(response)
                if round(np.array(poligonos)[:, 2].mean() * 100, 2) < MIN_QUALIDADE_OCR:
                    return make_response(jsonify(ERRO_BAIXA_QUALIDADE), 400)
                extraction = tex.line_segmentation(response, poligonos)
                boxes_line = reduce(lambda a, b: {**a, **b}, extraction)

                c, boxes, scale = 0, [], 1

                while len(boxes) < 35 and c < 10:
                    c += 1
                    scale += 0.1
                    image_norm = pro.process_image(cropped.copy(), 240, 70, scale)
                    x_min, y_min, x_max, y_max = pro.draw_box_around_text(cropped, poligonos)
                    image_norm = cv2.rectangle(image_norm, (x_min, y_min),
                                               (x_min + (x_max - x_min), y_min + (y_max - y_min)),
                                               (0, 0, 0), 3)
                    img_final = pro.complete_contours(image_norm, 40, 10)
                    contours = cv2.findContours(img_final, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[0]
                    contours, boxes = pro.filter_contours(contours, poligonos, image_norm, 0.0, 0.4)

                try:
                    boxes = reduce(lambda a, b: {**a, **b}, boxes)
                except Exception as e:
                    # raise
                    print(e)
                    return make_response(jsonify(ERRO_LEITURA), 400)
                if pro.draw_box_around_text(cropped, poligonos, is_complete=True):
                    return make_response(jsonify(ERRO_CANHOTO_PARCIAL), 400)

                im = plots.draw_boxes(contours, plots.polygons_image(cropped, poligonos, color=COR_POLIGONO_CAMPO))
                find_item, checado = {}, {}

                box_pairs = tex.find_pairs_box(boxes, boxes_line, poligonos)
                extracao = tex.find_uniques_pairs(box_pairs, boxes, boxes_line, poligonos)
                QRCODE = tex.find_qrcode(image_ori)
                for key, value in QRCODE.items():
                    extracao.update({max(extracao) + 1: {key: value}})
                output = {}
                if 'find_pair' in request.values.to_dict().keys():
                    output["BUSCA"] = {}
                    for item in eval(request.values["find_pair"]):
                        try:
                            max_key = {}
                            for pair in extracao:
                                for key, value in extracao[pair].items():
                                    max_key.update({pair: tex.match(key, item)})

                            better = \
                                max(filter(lambda x: x[1] > 79, [(n, max_key[n][1]) for n in max_key]),
                                    key=lambda x: x[1])[
                                    0]
                            output["BUSCA"].update(extracao[better])
                        except Exception as e:
                            # raise
                            print(e)
                            output["BUSCA"].update({tex.remove_accents(item): "NÃO ENCONTRADO"})

                if 'ancoras' in request.values.to_dict().keys():
                    output['COMPLETA'] = True
                    output['ANCORAS'] = {}
                    max_key = {}
                    for ancora in eval(request.values["ancoras"]):
                        output['ANCORAS'].update({ancora: {}})
                        for item in eval(request.values["ancoras"])[ancora]:
                            try:
                                for pair in extracao:
                                    for key, value in extracao[pair].items():
                                        max_key.update({pair: tex.match(key, item)})
                                better = max([(n, max_key[n][1], max_key[n][0]) for n in max_key], key=lambda x: x[1])
                                output['ANCORAS'][ancora].update({item: False if better[1] < 80 else True})
                            except Exception as e:
                                # raise
                                print(e)
                                output['ANCORAS'][ancora].update({item: False})
                        if not any(output['ANCORAS'][ancora].values()):
                            output.update({'COMPLETA': False})

                if 'return_img' in request.values.to_dict().keys():
                    if request.values["return_img"] == 'true':
                        image_b64 = b64encode(cv2.imencode('.png', cropped)[1]).decode()
                        output.update({'image': image_b64})

            except Exception as e:
                # raise
                print(e)
                return make_response(jsonify(ERRO_LEITURA), 400)

            output.update({'QRCODE': QRCODE})
            output.update({"OCR_CONFIDENCE": round(np.array(poligonos)[:, 2].mean() * 100, 2)})
            output.update({'PARES': [{key: value} for n in extracao for key, value in extracao[n].items()]})
            fim = time()
            output.update({'TIME': f"{round(fim - inicio, 2)}s"})
            return make_response(jsonify(output), 200)
        except Exception as e:
            # raise
            print(e)
            return make_response(jsonify(ERRO_LEITURA), 400)
