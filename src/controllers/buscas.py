import base64
import os
import shutil
from pathlib import Path
from time import time

import cv2
import numpy as np
import requests
from PIL import Image
from flask import current_app
from flask import jsonify, request, make_response
from flask_restplus import Resource, reqparse
from spacy import load

from models.buscas import busca
from server.instance import server
from utils import ocr, processing as pro, text_extraction as tex

app, api = server.app, server.api
nlp = load("AI_models/ner_model")

COR_POLIGONO_PALAVRA = (0, 0, 255)
COR_POLIGONO_CAMPO = (255, 125, 125)
COMPRIMENTO_PADRAO_IMAGEM = 1500
MIN_QUALIDADE_OCR = 68
ERRO_BAIXA_QUALIDADE = {"ERROR": "IMAGEM POSSUI BAIXA QUALIDADE"}
ERRO_LEITURA = {"ERROR": "FALHA AO FAZER LEITURA"}
ERRO_CONTEUDO_CANHOTO = {"ERROR": "IMAGEM DEVE CONTER APENAS O CONHOTO"}
ERRO_CANHOTO_PARCIAL = {"ERROR": "LEITURA PARCIAL DO CANHOTO"}

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"./credentials/GoogleVIsion.json"


@api.route('/busca')
class BookList(Resource):
    # @api.expect(canhoto, validate=True)
    # @api.marshal_with(canhoto, mask=False)
    @api.doc(parser=busca)
    def post(self):
        inicio = time()
        parser = reqparse.RequestParser()
        if 'image_url' in request.values.to_dict().keys():
            url = request.values["image_url"]
            path = os.path.join(current_app.config['UPLOAD_FOLDER'], 'image.jpeg')
            with open(path, 'wb') as out_file:
                shutil.copyfileobj(requests.get(url, stream=True).raw, out_file)
        else:
            file = request.files['image']
            path = os.path.join(current_app.config['UPLOAD_FOLDER'], file.filename)
            file.save(path)

        try:
            image_ = cv2.imread(path)
        except:
            image_ = np.asarray(Image.open(path))
        image_ori = image_

        cropped, response = ocr.align_image(image_, 1500, check=False)
        if response == 400:
            return make_response(jsonify(cropped), response)
        try:
            output = {}

            image_ori = image_

            poligonos = pro.find_polygons(response)
            image = cropped.copy()
            try:
                if os.path.isfile(path):
                    [f.unlink() for f in Path("./files").glob("*") if f.is_file()]
                    with open('./files/files.txt', 'w') as f:
                        f.write('')

                find_item, image_b64 = {}, {}

                if 'find' in request.values.to_dict().keys():
                    for item in eval(request.values["find"]):
                        try:
                            better = tex.match(response.text, item, validate=True)
                            if better[1] < 75:
                                find_item.update({item: {"FOUND_TEXT": "NÃO ENCONTRADO", "SIMILARIDADE": 0.0}})
                            else:
                                find_item.update({item: {"FOUND_TEXT": better[0], "SIMILARIDADE": better[1]}})
                        except Exception as e:
                            print(e)

                            find_item.update({item: {"FOUND_TEXT": "NÃO ENCONTRADO", "SIMILARIDADE": 0.0}})

                # if 'return_img' in request.values.to_dict().keys():
                #     if request.values["return_img"] == 'true':
                #         image_b64 = base64.b64encode(cv2.imencode('.png', image)[1]).decode()
                #         image_b64 = {'image': image_b64}

            except Exception as e:
                print(e)
                return make_response(jsonify(ERRO_LEITURA), 400)

            fim = time()
            output.update(find_item)
            output.update(image_b64)
            output.update({'COMPLETE_TEXT': response.text})
            output.update({"OCR_CONFIDENCE": round(np.array(poligonos)[:, 2].mean() * 100, 2)})
            output.update({'TIME': f"{round(fim - inicio, 2)}s"})

            return make_response(jsonify(output), 200)
        except Exception as e:
            print(e)
            return make_response(jsonify(ERRO_LEITURA), 400)
