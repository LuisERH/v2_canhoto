from math import degrees, atan2

import cv2
import numpy as np
from deskew import determine_skew
from shapely.geometry.polygon import Polygon

from utils import text_extraction


def getAngleOfLineBetweenTwoPoints(p1, p2):
    xDiff = p2[0] - p1[0]
    yDiff = p2[1] - p1[1]
    return degrees(atan2(yDiff, xDiff))


def is_upside_down(response):
    angles = []
    MIN_WORD_LENGTH_FOR_ROTATION_INFERENCE = 4
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    if len(word.symbols) < MIN_WORD_LENGTH_FOR_ROTATION_INFERENCE:
                        continue
                    letra = []
                    for symbol in word.symbols:
                        letra.append(symbol.text)
                    description = "".join(letra)
                    first_char = word.symbols[0]
                    last_char = word.symbols[-1]
                    first_char_center = (np.mean([v.x for v in first_char.bounding_box.vertices]),
                                         np.mean([v.y for v in first_char.bounding_box.vertices]))
                    last_char_center = (np.mean([v.x for v in last_char.bounding_box.vertices]),
                                        np.mean([v.y for v in last_char.bounding_box.vertices]))

                    p1 = word.bounding_box.vertices[0].x, word.bounding_box.vertices[0].y
                    p2 = word.bounding_box.vertices[1].x, word.bounding_box.vertices[1].y
                    angles.append(abs(getAngleOfLineBetweenTwoPoints(p1, p2)))
    angles = np.array(angles) < 90

    if len(np.where(angles == False)[0]) >= len(np.where(angles == True)[0]):
        return True
    else:
        return False


def detect_corners_from_contour(canvas, cnt):
    epsilon = 0.02 * cv2.arcLength(cnt, True)
    approx_corners = cv2.approxPolyDP(cnt, epsilon, True)
    approx_corners = sorted(np.concatenate(approx_corners).tolist())
    approx_corners = [approx_corners[i] for i in [0, 2, 1, 3]]
    return approx_corners


def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect


def four_point_transform(image, pts):
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    return warped


def biggestRectangle(contours):
    biggest = None
    max_area = 0
    indexReturn = -1
    for index in range(len(contours)):
        i = contours[index]
        area = cv2.contourArea(i)
        if area > 100:
            peri = cv2.arcLength(i, True)
            approx = cv2.approxPolyDP(i, 0.1 * peri, True)
            if area > max_area:  # and len(approx)==4:
                biggest = approx
                max_area = area
                indexReturn = index
    return indexReturn


def crop_img(image):
    try:
        if type(image) == str: image = cv2.imread(image)
        if len(image.shape) > 2:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            gray = image
        invGamma = 1.0 / 0.3
        table = np.array([((i / 255.0) ** invGamma) * 255
                          for i in np.arange(0, 256)]).astype("uint8")
        gray = cv2.LUT(gray, table)
        ret, thresh1 = cv2.threshold(gray, 70, 255, cv2.THRESH_BINARY)
        contours = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.ADAPTIVE_THRESH_GAUSSIAN_C)[0]
        indexReturn = biggestRectangle(contours)
        hull = cv2.convexHull(contours[indexReturn])
        hull = np.array([cv2.boxPoints(cv2.minAreaRect(hull))], dtype=np.int32)
        pts = detect_corners_from_contour(image, hull)
        return four_point_transform(image, np.array(pts))
    except Exception as e:
        # print(e);raise 
        return image


def rotate(image, background=(255, 255, 255)) -> np.ndarray:
    # grayscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # angle = determine_skew(grayscale)
    image_skew = noise_removal(image)
    angle = determine_skew(image_skew)
    if abs(angle) > 45 and abs(angle) < 90:
        angle = -(90 - abs(angle)) if angle > 0 else 90 - abs(angle)
    if abs(angle) >= 90: angle = abs(angle) % 90 if angle > 0 else -(abs(angle) % 90)
    height, width = image.shape[:2]
    image_center = (width / 2, height / 2)
    M = cv2.getRotationMatrix2D(image_center, angle, 1.)
    abs_cos = abs(M[0, 0])
    abs_sin = abs(M[0, 1])
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)
    M[0, 2] += bound_w / 2 - image_center[0]
    M[1, 2] += bound_h / 2 - image_center[1]
    rotated_mat = cv2.warpAffine(image, M, (bound_w, bound_h))
    return rotated_mat


def noise_removal(image):
    """Remove ruídos, tranforma a imagem em escala de cinza e devolve apenas a estrutura encontrada"""

    if len(image.shape) > 2: image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rgb_planes = cv2.split(image)
    result_norm_planes = []
    for plane in rgb_planes:
        dilated_img = cv2.dilate(plane, np.ones((3, 3), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 21)
        diff_img = 255 - cv2.absdiff(plane, bg_img)
        norm_img = cv2.normalize(diff_img, None, alpha=0, beta=230, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        result_norm_planes.append(norm_img)
    image = cv2.merge(result_norm_planes)

    return image


def clarify_text(image):
    """Aumenta o contraste das palavras"""
    if len(image.shape) > 2: image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.adaptiveThreshold(image,
                                  maxValue=255,
                                  adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                  thresholdType=cv2.THRESH_BINARY,
                                  blockSize=77,
                                  C=3)
    return image


def rotate_img(image):
    """rotaciona a imagem caso estiver torta"""
    image = clarify_text(image)
    coords = np.column_stack(np.where(image > 0))
    angle = cv2.minAreaRect(coords)[-1]
    angle = -(90 + angle) if angle < -45 else -angle
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    return cv2.warpAffine(image, M, (w, h),
                          flags=cv2.INTER_CUBIC,
                          borderMode=cv2.BORDER_REPLICATE)


def preprocess_image(image):
    """Preprocessamento da imagem completo"""
    image = rotate_img(image)
    return image


def find_polygons(response):
    polygons = []  # Polígonos que envolvem as palavras (oriundos da API do Google Vision)
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    letra = []
                    confidence = []
                    for symbol in word.symbols:
                        letra.append(symbol.text)
                        confidence.append(symbol.confidence)
                    description = "".join(letra)
                    points = word.bounding_box.vertices
                    polygon = Polygon(
                        [
                            [points[0].x, points[0].y],
                            [points[1].x, points[1].y],
                            [points[2].x, points[2].y],
                            [points[3].x, points[3].y],
                        ]
                    )
                    polygons.append((polygon, description, np.mean(confidence, dtype=np.float16)))
    return polygons


def process_image(image, v_kernel, h_kernel, scale=1):
    if len(image.shape) > 2: image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.bitwise_not(image)
    bw = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C,
                               cv2.THRESH_BINARY, 15, -2)
    horizontal_size = bw.shape[1] // int(h_kernel * (scale * 0.8))
    horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontal_size, 1))
    horizontal = cv2.erode(bw, horizontalStructure, iterations=2)
    horizontal = cv2.dilate(horizontal, horizontalStructure, iterations=2)
    # verticalsize = bw.shape[0] //  int(12*scale)
    verticalsize = bw.shape[0] // int(v_kernel * scale)
    verticalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, verticalsize))
    vertical = cv2.erode(bw, verticalStructure, iterations=2)
    vertical = cv2.dilate(vertical, verticalStructure, iterations=2)
    merged = ~cv2.addWeighted(vertical, 1., horizontal, 1., 0.0)
    img_final = cv2.erode(merged, (1, 1), iterations=2)
    return img_final


def FLD(image):
    fld = cv2.ximgproc.createFastLineDetector()
    lines = fld.detect(image)
    line_on_image = fld.drawSegments(image, lines)
    gray = cv2.cvtColor(np.array(line_on_image, dtype=np.uint8), cv2.COLOR_BGR2GRAY)
    return cv2.bitwise_not(cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)[1])


def complete_contours(image, x_line=120, y_line=23):
    """Completa os campos que são formados a partir do processamento de imag"""
    dst = cv2.cornerHarris(image, 2, 7, 0.04)
    thresh = np.float16(0.1 * dst.max())
    for y in range(dst.shape[0]):
        for x in range(dst.shape[1]):
            if dst[y, x] > thresh:
                cv2.rectangle(image, (x - 1, y - y_line), (x - 1, y + y_line), (0, 0, 0), 1)
                cv2.rectangle(image, (x - x_line, y - 1), (x + x_line, y - 1), (0, 0, 0), 1)
    image = ~FLD(image)
    return image


def get_contour_precedence(contour, cols):
    tolerance_factor = 10
    origin = cv2.boundingRect(contour)
    return ((origin[1] // tolerance_factor) * tolerance_factor) * cols + origin[0]


def sort_contours(contours, image):
    contours.sort(key=lambda x: get_contour_precedence(x, image.shape[1]))
    return contours


def filter_contours(contours, poligonos, image, min_percent=0.0006, max_percent=0.095, canhoto=False):
    indice = 1
    img_width, img_height = image.shape[1], image.shape[0]
    valid_contours, textos_caixa = [], []
    contours = list(
        filter(
            lambda c: cv2.contourArea(c) > img_width * img_height * min_percent
                      and cv2.contourArea(c) < img_width * img_height * max_percent,
            contours,
        )
    )

    contours = sort_contours(contours, image)

    for contour in contours:
        rect = cv2.minAreaRect(contour)
        four_coords = np.array(cv2.boxPoints(rect), dtype=np.int16)
        top_left_x, top_left_y, bot_right_x, bot_right_y = min(four_coords[:, 0]), min(four_coords[:, 1]), max(
            four_coords[:, 0]), max(four_coords[:, 1])
        crop = image[top_left_y:bot_right_y, top_left_x:bot_right_x]

        output = text_extraction.find_in_box(np.array([four_coords], dtype=np.int32), poligonos, indice)

        if crop.size > max_percent * image.size: output = ""
        h_max = image.shape[0] * 0.89 if canhoto == True else image.shape[0] * 1.0
        w_max = image.shape[1] * 0.85 if canhoto == True else image.shape[1] * 1.0
        if output != "" and crop.shape[1] < w_max and crop.size > min_percent * image.size and crop.shape[0] < h_max:
            indice += 1
            valid_contours.append(contour)
            textos_caixa.append(output)
    return valid_contours, textos_caixa


def draw_box_around_text(image, poligonos, is_complete=False):
    poligonos = list(filter(lambda p: p[1] not in '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', poligonos))

    xs_min = sorted([min(np.array(poly.exterior.coords[:4])[:, 0]) for poly in np.array(poligonos)[:, 0]])
    xs_max = sorted([max(np.array(poly.exterior.coords[:4])[:, 0]) for poly in np.array(poligonos)[:, 0]])
    ys_min = sorted([min(np.array(poly.exterior.coords[:4])[:, 1]) for poly in np.array(poligonos)[:, 0]])
    ys_max = sorted([max(np.array(poly.exterior.coords[:4])[:, 1]) for poly in np.array(poligonos)[:, 0]])
    x_min, x_max, y_min, y_max = int(xs_min[0]) - 5, int(xs_max[-1] + 20), int(ys_min[0]) - 1, int(ys_max[-1] + 20)
    if y_max > image.shape[0] - 10: y_max = image.shape[0] - 10
    if x_max > image.shape[1]: x_max = image.shape[1] - 10
    if y_min < 0: y_min = 10
    if x_min < 0: x_min = 10
    if is_complete:
        return True if abs(xs_max[-1] - xs_min[0]) < 1050 else False
    return x_min, y_min, x_max, y_max


def resize(image, size, flag='W'):
    """ Valida imagens que contém a proporção de 20%/80% (similiares a medidas de um canhoto)

    Args:
        image (np.array): Imagem.
        size (int): Comprimento da Imagem.
    Returns:
        image (np.array): Imagem redimensionada.

    """
    h, w = image.shape[:2]
    orientation = w if flag == "W" else h
    resi = np.divide(size, orientation, dtype=np.float16)
    resized = cv2.resize(
        image, None, fx=resi, fy=resi, interpolation=cv2.INTER_LINEAR_EXACT
    )
    return resized


def check_proportion(shape, orientation=True):
    """ Valida imagens que contém a proporção de 30%/70% (similiares a medidas de um canhoto)

    Args:
        shape (tuple): Dimensões da imagem
        orientation(bool): Se desejar retornar a orientação da imagem
    Returns:
        bool: True, se atende a proporção correta senão False
        str: HORIZONTAL ou VERTICAL se orientation for True

    """
    height, width = shape[0], shape[1]
    proportion = height / (height + width) * 100, abs(height / (height + width) * 100 - 100)
    correct = True if min(proportion) > 7 and min(proportion) < 30 else False
    if orientation:
        return (correct, "HORIZONTAL") if proportion[0] < proportion[1] else (correct, "VERTICAL")
    else:
        return correct
