import re
from copy import deepcopy
from datetime import datetime
from difflib import SequenceMatcher
from string import digits, punctuation
import cv2
import numpy as np
import unicodedata
from dateparser import parse
from dateparser.search import search_dates
from nltk import everygrams
from pyzbar import pyzbar
from shapely.geometry import Polygon, Point, LineString
from validate_docbr import CPF

from utils import processing

cpf = CPF()


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()


def get_lines_from_box(box, boxes_line, poligonos):
    """ Encontra todas as linhas  pertencentes a caixa delimitadora.

    Args:
        poligonos (list): Caixas delimitadoras de cada palavra no documento.
        box (dict): Conteúdo de uma caixa delimitadora.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict : Linhas que estão dentro da caixa.

    """
    boxes_linha = deepcopy(boxes_line)
    poli_box = Polygon(box['bounding_box_block'])
    lines = []

    for line in boxes_linha:
        poli_line = Polygon(boxes_linha[line]['bounding_box_block'])
        if poli_box.intersects(poli_line):
            remove = []
            for i, word in enumerate(boxes_linha[line]['words']):  # cada palavra na linha que corta a caixa
                if not poli_box.contains(
                        Polygon(word['bounding_box']).centroid):  # se a caixa contem a palavra na linha
                    # boxes_linha[line]['words']
                    remove.append(i)
            for index in sorted(remove, reverse=True):
                del boxes_linha[line]['words'][index]
            lines.append(line)
        boxes_linha[line]['box_full_text'] = ' '.join([w['description'] for w in boxes_linha[line]['words']])

    boxes_line = {key: value for key, value in boxes_linha.items() if key in lines}
    new_lines = {}
    for p, w, d in poligonos:
        if poli_box.contains(p.centroid):
            for l in boxes_line:
                if not Polygon(boxes_linha[l]['bounding_box_block']).contains(p.centroid):
                    new_lines.update(find_in_box(p.exterior.coords[:4], poligonos,
                                                 max([*boxes_line.keys(), *new_lines.keys()]) + 1))
    boxes_line.update(new_lines)
    return boxes_line


def get_lines_from_box2(box, boxes_line):
    """ Encontra todas as linhas  pertencentes a caixa delimitadora.

    Args:
        box (dict): Conteúdo de uma caixa delimitadora.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict : Linhas que estão dentro da caixa.

    """
    boxes_linha = deepcopy(boxes_line)
    poli_box = Polygon(box['bounding_box_block'])
    lines = []

    for line in boxes_linha:
        poli_line = Polygon(boxes_linha[line]['bounding_box_block'])
        if poli_box.intersects(poli_line):
            remove = []
            for i, word in enumerate(boxes_linha[line]['words']):  # cada palavra na linha que corta a caixa
                if not poli_box.contains(
                        Polygon(word['bounding_box']).centroid):  # se a caixa contem a palavra na linha
                    # boxes_linha[line]['words']
                    remove.append(i)
            for index in sorted(remove, reverse=True):
                del boxes_linha[line]['words'][index]
            lines.append(line)
    return {key: value for key, value in boxes_linha.items() if key in lines}


def find_value_by_key(alvo, box, boxes_line, poligonos, flag="RIGHT"):
    """ Encontra o par correspondente a chave enviada.

    Args:
        poligonos (list): Caixas delimitadoras de cada palavra no documento.
        box (dict): Conteúdo de uma caixa delimitadora.
        boxes_line(dict): Todas as linhas presentes no documento .
        alvo (str): Chave desejada.
        flag(str): Representa a horientação do par, RIGHT para horizontal e BOTTOM para vertical.
    Returns:
        dict : Par de chave e valor correspondentes

    """
    palavras = box['words']  # lista de palavras na caixa
    bounds_box = np.array(box['bounding_box_block'], dtype='uint16')
    texto = " ".join(word['description'] for word in palavras)
    matched = match(texto, alvo)
    alvo_encontrado = max(palavras, key=lambda p: match(p['description'], matched[0].split()[-1])[1])

    if matched[1] < 78: return
    bounds_alvo = np.array(alvo_encontrado['bounding_box'], dtype='uint16')
    ma_y, mi_y, mi_x, ma_x = bounds_box[:, 1].max(), bounds_box[:, 1].min(), bounds_box[:, 0].min(), bounds_box[:,
                                                                                                     0].max()
    max_y, min_y, min_x, max_x = bounds_alvo[:, 1].max(), bounds_alvo[:, 1].min(), bounds_alvo[:, 0].min(), bounds_alvo[
                                                                                                            :, 0].max()
    ponto = Point(min_x, (np.mean([min_y, max_y], dtype=np.uint16)))
    ponto2 = Point(ma_x, (np.mean([min_y, max_y], dtype=np.uint16)))
    h, _ = polygon_width(Polygon(alvo_encontrado['bounding_box']))

    # right =  Polygon([[min_x,min_y],[ma_x,min_y],[ma_x,max_y],[min_x,max_y]])
    right = LineString([ponto, ponto2])
    bottom = Polygon([[min_x, min_y], [max_x, min_y], [max_x, max_y + 2 * h], [min_x, max_y + 2 * h]])
    projecao = right if flag == 'RIGHT' else bottom
    lines = get_lines_from_box(box, boxes_line, poligonos)
    value, count = '', 0

    for line in lines:
        if projecao.intersects(Polygon(lines[line]['bounding_box_block'])):
            count += 1
            if flag == "RIGHT" and count < 3:
                value += f"{lines[line]['box_full_text']} "
            if flag == "BOTTOM" and count == 2:
                value += f"{lines[line]['box_full_text']} "
    return {matched[0]: value.replace(matched[0], '').strip()}


def find_pairs_box(boxes, boxes_line, poligonos):
    """ Encontra as caixas cujo possuem pares de informação com uma ou várias linhas.

    Args:
        boxes(dict): Caixas delimitadoras.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict : índices das caixas cujo possuem  um único par de informações, e caixas que contém vários pares.
    """
    pares_unicos, multiplos_pares = [], []
    for box in boxes:
        count_lines = len(get_lines_from_box2(boxes[box], boxes_line))
        if count_lines in [1, 2, 3]:
            pares_unicos.append(box)
        elif count_lines > 3:
            multiplos_pares.append(box)
    return {'uniques': pares_unicos, 'multiples': multiplos_pares}


def find_uniques_pairs(box_pairs, boxes, boxes_line, poligonos):
    """ Encontra todos os pares de informação contidos nas caixas (parâmetro boxes).

    Args:
        box_pairs: Retorno de "find_pairs_box".
        boxes(dict): Caixas delimitadoras.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict: Linhas que estão dentro da caixa.
    """
    pares = {}
    for box in box_pairs['uniques']:  # Campos que contêm 2 linhas
        poli_box = Polygon(boxes[box]['bounding_box_block'])
        lines = get_lines_from_box2(boxes[box], boxes_line)
        poli_line = Polygon(lines[list(lines.keys())[0]]['bounding_box_block'])
        textos = []
        [textos.append(" ".join(word['description'] for word in lines[l]['words'])) for l in lines]

        if len(textos) == 1:
            if polygon_width(poli_line)[0] > polygon_width(poli_line)[0] * 2.5:
                textos.append('')
            else:
                x = poli_box.centroid.coords[0][0]
                y = poli_box.centroid.coords[0][1] + polygon_width(poli_box)[0]
                for box2 in boxes:
                    poli_box2 = Polygon(boxes[box2]['bounding_box_block'])
                    if poli_box2.contains(Point(x, y)) and polygon_width(poli_box2)[1] > polygon_width(poli_box)[
                        1] * 0.8:
                        if polygon_width(poli_box2)[1] < polygon_width(poli_box)[1] * 1.2:
                            textos.append(boxes[box2]['box_full_text'])

        if len(textos) == 2:
            pares.update({box: {remove_accents(textos[0]): textos[1]}})
        elif len(textos) == 3:
            pares.update({box: {remove_accents(textos[0]): f'{textos[1]}, {textos[2]}'}})

    valores = [list(pares[i].values())[0] for i in pares]
    for i in pares.copy():
        if list(pares[i].values())[0] == '' and list(pares[i].keys())[0] in valores:
            del pares[i]
    return pares


def find_nota1(boxes_nota, boxes_line, image, poligonos):
    """ Encontra o número da nota fiscal e a série com base nas coordenadas

    Args:
        poligonos (list): Caixas delimitadoras de cada palavra no documento.
        image: Imagem pré-processada.
        boxes_nota: Caixas delimitadas pelos contronos da imagem.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict : Valores para NOTA_FISCAL e SERIE
    """
    try:
        boxes_nota = boxes_nota.copy()
        keys = []
        for ind in boxes_nota:  # percorre cada retangulo (box)
            bounds = np.array(boxes_nota[ind]['bounding_box_block'])
            if min(bounds[:, 0]) < image.shape[1] * 0.6: keys.append(ind)  # se o X mínimo for menor que 60% da imagem
        entries_to_remove(keys, boxes_nota)  # remove os elementos

        caixas = sorted(boxes_nota,
                        key=lambda c: max(np.array(boxes_nota[c]['bounding_box_block'])[:, 0]),
                        reverse=True)
        caixas = list(filter(lambda c: len(''.join(re.findall(r'[0-9]+', boxes_nota[c]['box_full_text']))) > 3, caixas))

        index_nota = caixas[0]

        texto = remove_accents(boxes_nota[index_nota]['box_full_text'])
        matche = match(texto, "SERIE")
        candidatas = []
        caixa_serie, serie = '', ''
        poli = Polygon(boxes_nota[index_nota]['bounding_box_block'])
        boxes_line = get_lines_from_box(boxes_nota[index_nota], boxes_line, poligonos)
        for line in boxes_line:
            if poli.intersects(Polygon(boxes_line[line]['bounding_box_block']).centroid):
                texto_line = remove_accents(boxes_line[line]['box_full_text'])
                if match(texto_line, "SERIE")[1] > 85:
                    caixa_serie = boxes_line[line]
                    if ''.join(re.findall(r'[0-9]+', texto_line)) != '':
                        serie = ''.join(re.findall(r'[0-9]+', texto_line))
                numeros = ''.join(re.findall(r'[0-9]+', boxes_line[line]['box_full_text']))
                if numeros != '':
                    candidatas.append([numeros, boxes_line[line]])
        nota = max(candidatas, key=lambda x: len(x[0]))[0]

        candidatas = list(filter(lambda x: x[0] != nota, candidatas))
        if serie == '' and caixa_serie != '':
            poly = Polygon(caixa_serie['bounding_box_block'])
            try:
                serie = sorted(candidatas, key=lambda b: poly.distance(Polygon(b[1]['bounding_box_block'])))[0][0]
            except Exception as e:
                # print(e);raise
                serie = ''

        if nota == '':
            return {"NOTA_FISCAL": "NÃO ENCONTRADO"}
        elif nota != '' and serie == '':
            return {"NOTA_FISCAL": nota}
        elif nota == '' and serie == '':
            return {"NOTA_FISCAL": "NÃO ENCONTRADO"}

        return {"NOTA_FISCAL": nota, "SERIE": serie}
    except Exception as e:
        # print(e);raise

        return {"NOTA_FISCAL": "NÃO ENCONTRADO"}


def clean_text(text):
    """ Faz a limpeza de ruídos no texto

    Args:
        text (str): String com ruídos.
    Returns:
        str : String limpa.
    """
    text = remove_accents(text)
    text = re.sub(r'[^\w\s]', '', text)
    text = re.sub(' +', ' ', text)
    text = text.replace('\n', ' ')
    text = text.strip()
    text = f' {text} '
    return text


def find_nota3(texto, nlp):
    """ Encontra o número da nota fiscal e a série com base na extração do OCR

    Args:
        texto (str): String com o texto extraído pelo OCR.
        nlp: Modelo de NER para extração personalizada.
    Returns:
         dict : Valores para NOTA_FISCAL e SERIE
    """
    output = {}
    canhoto = nlp(clean_text(texto))
    if len(canhoto.ents) > 0:
        for word in canhoto.ents:
            output.update({word.label_: ''.join(re.findall(r"[0-9]+", word.text))})
        return output
    else:
        return {"NOTA_FISCAL": "NÃO ENCONTRADO"}


def find_qrcode(image):
    """ Extrai dados do QRCODE ou BARCODE de uma imagem.

    Args:
        image: imagem para extrair código QRCODE/BARCODE.
    Returns:
         dict : Valores extraídos do QRCODE/BARCODE.
    """
    try:
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        decoded = pyzbar.decode(gray)
        numbers = []
        if decoded:
            for d in decoded:
                numbers.extend(re.findall(r'\b\d+\b', d.data.decode('utf-8')))
                cv2.rectangle(image, (d.rect[0], d.rect[1]), (d.rect[0] + d.rect[2], d.rect[1] + d.rect[3]),
                              (0, 0, 255), 2)
        idmov = list(filter(lambda x: len(x) == 44, numbers))[0]
        UF, DATA, CNPJ, MODELO, SERIE, NUMERO, CODIGO, VERIFICADOR = idmov[:2], idmov[2:6], idmov[6:20], idmov[
                                                                                                         20:22], idmov[
                                                                                                                 22:25], idmov[
                                                                                                                         25:34], idmov[
                                                                                                                                 34:43], \
                                                                     idmov[-1]
        DATA = '{}/20{}'.format(DATA[2:], DATA[:2])
        CNPJ = '{}.{}.{}/{}-{}'.format(CNPJ[:2], CNPJ[2:5], CNPJ[5:8], CNPJ[8:12], CNPJ[12:])

        return {"UF": UF, "DATA": DATA, "NOTA_FISCAL": NUMERO, "SERIE": SERIE, "MODELO": MODELO, "CNPJ": CNPJ,
                "CODIGO": CODIGO, "VERIFICADOR": VERIFICADOR}
    except Exception as e:
        # print(e);raise

        return {}


def find_nota(boxes, boxes_line, cropped, image_ori, texto, poligonos, nlp, use_nlp=False):
    """ Encontra o número da nota fiscal e a série com base nas coordenadas, texto puro ou QRCODE.

    Args:
        use_nlp (bool): True para utilizar somente o modelo de linguagem.
        texto (str): String com o texto extraído pelo OCR.
        nlp: Modelo de NER para extração personalizada.
        image_ori: imagem original.
        cropped: imagem pré-processada.
        boxes(dict): Caixas delimitadoras.
        poligonos (list): Caixas delimitadoras de cada palavra no documento.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict : Valores para NOTA_FISCAL e SERIE.
    """
    output = {'NOTA_FISCAL': "", 'SERIE': ''}
    try:
        qrcode = find_qrcode(image_ori)
        if qrcode:
            return {"NOTA_FISCAL": qrcode['NOTA_FISCAL'], "SERIE": qrcode['SERIE']}

        if not use_nlp:
            output.update(find_nota1(boxes, boxes_line, cropped, poligonos))
        else:
            output["NOTA_FISCAL"] = "NÃO ENCONTRADO"

        if len(output['NOTA_FISCAL']) < len(output['SERIE']):
            output['SERIE'], output['NOTA_FISCAL'] = output['NOTA_FISCAL'], output['SERIE']

        if len(output["SERIE"]) > 3: output["SERIE"] = ""
        if output['NOTA_FISCAL'] == output['SERIE']: output["SERIE"] = ""
        if output["NOTA_FISCAL"] not in ["NÃO ENCONTRADO", '']:
            if len(output["NOTA_FISCAL"]) < 4 or len(output["NOTA_FISCAL"]) > 11:
                output.update({'NOTA_FISCAL': "NÃO ENCONTRADO"})
        if output["NOTA_FISCAL"] in ["NÃO ENCONTRADO", '']:
            output["SERIE"] = ""
            output.update(find_nota3(texto, nlp))

        if len(output['NOTA_FISCAL']) < len(output['SERIE']):
            output['SERIE'], output['NOTA_FISCAL'] = output['NOTA_FISCAL'], output['SERIE']
        if len(output["SERIE"]) > 3: output["SERIE"] = ""
        if output['NOTA_FISCAL'] == output['SERIE']: output["SERIE"] = ""
        if output["NOTA_FISCAL"] in ["NÃO ENCONTRADO", '']: output["SERIE"] = ""
        if output["NOTA_FISCAL"] not in ["NÃO ENCONTRADO", '']:
            if len(output["NOTA_FISCAL"]) < 4 or len(output["NOTA_FISCAL"]) > 11:
                output.update({'NOTA_FISCAL': "NÃO ENCONTRADO"})

        if output['SERIE'] == '': del output["SERIE"]
        if output["NOTA_FISCAL"] not in ["NÃO ENCONTRADO", ''] and texto.find(" SERIE O ") != -1:
            output['SERIE'] = '0'
    except Exception as e:
        # print(e);raise

        output.update({'NOTA_FISCAL': "NÃO ENCONTRADO"})
    return output


def find_date(boxes, boxes_line, image, response):
    """ Encontra o número da nota fiscal e a série com base nas coordenadas, texto puro ou QRCODE.

    Args:
        response : Retorno do Google Vision OCR.
        image: imagem.
        boxes(dict): Caixas delimitadoras.
        boxes_line(dict): Todas as linhas presentes no documento .
    Returns:
        dict : Valores para NOTA_FISCAL e SERIE.
    """
    try:
        ascii_letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        compare = "DATA DE RECEBIMENTO"
        index = max([[match(boxes[index]['box_full_text'], compare)[1], index] for index in boxes])[1]
        date = parse(re.sub(str(list(ascii_letters)), '', boxes[index]['box_full_text']).strip(),
                     languages=['pt'],
                     locales=['pt-AO'],
                     date_formats=['%d %m %Y'])
        if date is not None:
            date, year = date.strftime("%d/%m/%Y"), datetime.now().year
            if int(date.split('/')[-1]) < 2010: return ""
            if int(date.split('/')[-1]) > year:
                return {"DATE": date.replace(date[6:10], str(year))}
            return {"DATE": date}
        else:
            found = re.search(r"(\d+/\d+/\d+)", response.text)
            if found is not None:
                return {"DATE": found.group(1)}
        return {"DATE": ""}
    except Exception as e:
        # print(e);raise

        return {"DATE": ""}


def find_text_in_box(block, text_block):
    block = Polygon(block)
    texto = []
    for index in text_block:
        poly = Polygon(text_block[index]['bounding_box_block'])
        if block.contains(poly.centroid):
            texto.append(text_block[index]['box_full_text'])
    return texto


def entries_to_remove(entries, the_dict):
    for key in entries:
        if key in the_dict:
            del the_dict[key]


def bounding_box_to_contour(bounding_box, scale=1):
    """ Converte coordenadas para po padrão de contorno do openCV.

    Args:
        bounding_box : 4 coordenadas formando uma caixa (padrão [(x, y), (x, y), (x, y), (x, y)])
        scale (float): Amplia ou contrai o polígono em questão.
    Returns:
        dict : Contorno escalonado no padrão openCV.
    """
    contour = np.array([[c] for c in bounding_box], dtype='int32')
    M = cv2.moments(contour)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    contour_norm = contour - [cx, cy]
    contour_scaled = contour_norm * scale
    contour_scaled = contour_scaled + [cx, cy]
    contour_scaled = contour_scaled.astype(np.int32)
    return contour_scaled


def is_signed(boxes, boxes_line, image):
    try:
        boxes_crop = boxes.copy()
        image = processing.clarify_text(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
        compare, keys = "IDENTIFICAÇÃO E ASSINATURA DO RECEBEDOR", set()
        for ind in boxes_crop:
            bounds = np.array(boxes_crop[ind]['bounding_box_block'])
            if max(bounds[:, 0]) - min(bounds[:, 0]) > image.shape[1] / 1.2: keys.add(ind)
            if min(bounds[:, 1]) < image.shape[0] / 2.5: keys.add(ind)
        entries_to_remove(keys, boxes_crop)

        index = max([[match(boxes_crop[index]['box_full_text'], compare)[1], index] for index in boxes_crop])
        box_line = max([[match(boxes_line[index]['box_full_text'], compare)[1], index] for index in boxes_line])[1]
        if index[0] < 70: return {"ASSINADO": True}
        index = index[1]
        bounds_line = np.array(boxes_line[box_line]['bounding_box_block'], dtype=np.int16)
        bounds_crop = np.array(boxes_crop[index]['bounding_box_block'], dtype=np.int16)
        top_left_x, top_left_y, bot_right_x, bot_right_y = min(bounds_crop[:, 0]), min(bounds_crop[:, 1]), max(
            bounds_crop[:, 0]), max(bounds_crop[:, 1])
        contour = bounding_box_to_contour(boxes_line[box_line]['bounding_box_block'], 1.2)
        if (bot_right_y - top_left_y) / 1.1 < contour[:, :, 1].max() - contour[:, :, 1].min():
            return {"ASSINADO": True}
        cv2.drawContours(image, [contour], 0, (255, 255, 255), -1, )
        crop = image[top_left_y + 6:bot_right_y - 6, top_left_x + 6:bot_right_x - 6]
        if crop.shape[1](max(bounds_line[:, 1]) - min(bounds_line[:, 1])) / 2:
            return {"ASSINADO": True} if np.sum(crop == 0) > 300 else {"ASSINADO": False}
        else:
            return {"ASSINADO": True}
    except Exception as e:
        # print(e);raise

        return {"ASSINADO": True}


def is_dated(boxes, boxes_line, image):
    try:
        boxes_crop = boxes.copy()
        image = processing.clarify_text(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
        compare, keys = "DATA DE RECEBIMENTO", set()
        for ind in boxes_crop:
            bounds = np.array(boxes_crop[ind]['bounding_box_block'])
            if max(bounds[:, 0]) - min(bounds[:, 0]) > image.shape[1] / 3: keys.add(
                ind)  # Campor precisa ser menor que 1/3 da imagem
            if min(bounds[:, 0]) > image.shape[1] / 3: keys.add(
                ind)  # Início do campo precisa estar antes do primeiro 1/3 da imagem
        entries_to_remove(keys, boxes_crop)  # remover o que está fora da condição

        index = max([[match(boxes_crop[index]['box_full_text'], compare)[1], index] for index in boxes_crop])

        if index[0] < 70:
            compare = "DATA E HORA"
            index = max([[match(boxes_crop[index]['box_full_text'], compare)[1], index] for index in boxes_crop])

        if index[0] < 70: return {"DATADO": True}

        box_line = max([[match(boxes_line[index]['box_full_text'], compare)[1], index] for index in boxes_line])[1]
        index = index[1]
        bounds_line = np.array(boxes_line[box_line]['bounding_box_block'], dtype=np.int16)
        bounds_crop = np.array(boxes_crop[index]['bounding_box_block'], dtype=np.int16)
        top_left_x, top_left_y, bot_right_x, bot_right_y = min(bounds_crop[:, 0]), min(bounds_crop[:, 1]), max(
            bounds_crop[:, 0]), max(bounds_crop[:, 1])

        contour = bounding_box_to_contour(boxes_line[box_line]['bounding_box_block'], 1.2)
        if (bot_right_y - top_left_y) / 1.1 < contour[:, :, 1].max() - contour[:, :, 1].min():
            return {"DATADO": True}
        cv2.drawContours(image, [contour], 0, (255, 255, 255), -1, )
        crop = image[top_left_y + 6:bot_right_y - 6, top_left_x + 6:bot_right_x - 6]
        if crop.shape[1] > (max(bounds_line[:, 1]) - min(bounds_line[:, 1])) / 2:
            return {"DATADO": True} if np.sum(crop == 0) > 300 else {"DATADO": False}
        else:
            return {"DATADO": True}
    except Exception as e:
        # print(e);raise

        return {"DATADO": True}


def find_in_box(box, poligonos, indice):
    box = Polygon(np.squeeze(box))
    caixas, texto_conf, caixas_texto = [], [], []
    output = []
    for polygon, desc, conf in poligonos:
        if box.contains(polygon.centroid):
            four_points = np.array([polygon.exterior.coords[:4]], dtype=np.int32)
            caixas.append(four_points)
            caixas_texto.append([four_points, desc, conf])
    if len(caixas) == 0:
        return ""  # caso o contorno do campo não conter nenhum texto

    for four_points, desc, conf in caixas_texto:
        texto_conf.append((desc, four_points))
    texto_conf = np.array(texto_conf, dtype='object')

    for four_points, desc, conf in caixas_texto:
        output.append(
            {
                "description": str(desc),
                "confidence": float(conf),
                "bounding_box": np.squeeze(four_points).tolist(),
            }
        )

    output = {
        indice: {
            "box_full_text": " ".join(texto_conf[:, 0]),
            "bounding_box_block": box.exterior.coords[:4],
            "words": output,
        }
    }
    return output


def sort_words(words):
    bound = words['bounding_box_block']
    max_y, min_y, min_x = int(max([i[1] for i in bound])), int(min([i[1] for i in bound])), int(
        min([i[0] for i in bound]))
    ponto = Point(min_x, (np.mean([min_y, max_y], dtype=np.float16)))
    sorted_words = []
    for word in words['words']:
        polygon = Polygon(word['bounding_box'])
        sorted_words.append((ponto.distance(polygon),
                             word['description'],
                             word['confidence'],
                             polygon))
    return [(round(s[0], 2), s[1], s[2], s[3]) for s in sorted(sorted_words, key=lambda w: w[0])]


def stop_calc2(sorted_words):
    "compara o espaço entre cada palavra (objetivo disto é saber quando termina a informação que desejamos extrair"
    distance = []
    [distance.append(sorted_words[i][3].distance(sorted_words[i - 1][3])) for i in range(2, len(sorted_words))]
    return [i + 1 for i, j in enumerate(distance) if j > 54]


def clean_word(word, label, mat):
    remove_digits = str.maketrans('', '', digits)
    remove_puncts = str.maketrans('', '', punctuation)
    if match(word, 'DIGITALIZADO')[1] > 80: return ''
    if match(word, 'PESQUISADO')[1] > 80: return ''
    word = word.translate(remove_digits).translate(remove_puncts).replace(mat[0], '').strip()
    return word


def get_name(boxes, label):
    candidates = []
    for i, box in enumerate(boxes, 1):
        bounds = np.array(box[i]['bounding_box_block'], dtype='uint16')
        max_y, min_y, min_x = bounds[:, 1].max(), bounds[:, 1].min(), bounds[:, 0].min()
        ponto = Point(min_x, (np.mean([min_y, max_y], dtype=np.float16)))
        sorted_words = []
        for word in box[i]['words']:
            polygon = Polygon(word['bounding_box'])
            sorted_words.append((ponto.distance(polygon),
                                 word['description'],
                                 word['confidence'],
                                 polygon))

        sorted_words = [(round(s[0], 2), remove_accents(s[1]), s[2], s[3]) for s in
                        sorted(sorted_words, key=lambda w: w[0])]
        quebra = stop_calc2(sorted_words)
        sorted_words = sorted_words if len(quebra) == 0 else sorted_words[:quebra[0] + 1]
        mat = match(sorted_words[0][1], label)
        if mat[1] > 77:
            del sorted_words[0]
            candidates.append(np.array([[clean_word(desc, label, mat), conf]
                                        for _, desc, conf, _ in sorted_words
                                        if clean_word(desc, label, mat) != '']))

    result = [(' '.join(c[:, 0]), np.mean(c[:, 1].astype(np.float16))) for c in candidates if
              c.size > 0 and len(c[:, 0]) > 1 and len(c[:, 0]) < 8]
    return max(result) if len(result) > 0 else result


def stop_calc(sorted_words, segments):
    "compara o espaço entre cada palavra (objetivo disto é saber quando termina a informação que desejamos extrair"
    distance = []
    [distance.append(sorted_words[i][3].distance(sorted_words[i - 1][3])) for i in range(1, len(sorted_words))]
    output = [i + 1 for i, j in enumerate(distance) if j > 54]
    output.append(len(sorted_words))
    mark = 0
    for quebra in output:
        segments.append(sorted_words[mark:quebra])
        mark = quebra
    return segments


def line_segmentation(response, poligonos):
    median = int(np.median([polygon_width(p)[0] for p in np.array(poligonos)[:, 0] if polygon_width(p) != None]))
    simbolos = []
    for page in response.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                palavra = []
                for word in paragraph.words:
                    for symbol in word.symbols:
                        nada = symbol
                        simbolos.append(symbol)
    breaks, output = [], []
    starts = [simbolos[0]]
    for i in range(len(simbolos)):
        try:
            b = simbolos[i].property.detected_break.type_.name
            if b == "EOL_SURE_SPACE" or b == "LINE_BREAK":
                breaks.append(simbolos[i])
                starts.append(simbolos[i + 1])
        except Exception as e:
            # print(e);raise

            pass

    for indice, (s, b) in enumerate(zip(starts, breaks), 1):
        try:
            s = s.bounding_box.vertices
            b = b.bounding_box.vertices

            if np.diff((s[0].x, b[0].x)) > 0 and np.diff((s[0].x, b[0].x)) < median * 0.3:
                linhas = find_in_box(np.array([[b[1].x, b[1].y],
                                               [b[2].x, b[2].y],
                                               [s[3].x, s[3].y],
                                               [s[0].x, s[0].y]], dtype=np.int32),
                                     poligonos,
                                     indice)

            else:
                linhas = find_in_box(np.array([[s[0].x, s[0].y],
                                               [b[1].x, b[1].y],
                                               [b[2].x, b[2].y],
                                               [s[3].x, s[3].y]], dtype=np.int32),
                                     poligonos,
                                     indice)
            if len(linhas) == 0:
                pass
            else:
                output.append(linhas)
        except Exception as e:
            # print(e);raise

            continue
    return output


def camp_segmentation(boxes):
    boxes_test = boxes.copy()
    n = 0
    for i in boxes:
        segments = []
        sorted_words = sort_words(boxes[i])
        segments = stop_calc(sorted_words, segments)

        for segment in segments:
            output = []
            n += 1
            y_max = np.array(segment[0][3].exterior.coords[:4], dtype=np.int32)[:, 1].max()
            y_min = np.array(segment[0][3].exterior.coords[:4], dtype=np.int32)[:, 1].min()
            x_min = np.array(segment[0][3].exterior.coords[:4], dtype=np.int32)[:, 0].min()

            y_max_ = np.array(segment[-1][3].exterior.coords[:4], dtype=np.int32)[:, 1].max()
            y_min_ = np.array(segment[-1][3].exterior.coords[:4], dtype=np.int32)[:, 1].min()
            x_max_ = np.array(segment[-1][3].exterior.coords[:4], dtype=np.int32)[:, 0].max()

            boxes_test[n] = {'box_full_text': " ".join([i[1] for i in segment])}
            boxes_test[n]['bounding_box_block'] = [(x_min, y_min), (x_max_, y_min_), (x_max_, y_max_), (x_min, y_max)]
            for seg in segment:
                output.append({"description": seg[1],
                               "confidence": seg[2],
                               "bounding_box": seg[3].exterior.coords[:4]})
            boxes_test[n]['words'] = output

    return boxes_test


def match(texto_ocr, alvo, min_words=1, validate=False):
    try:
        if len(alvo.split()) > 2: min_words = len(alvo.split()) - 2
        texto_ocr = remove_accents(texto_ocr)
        alvo = remove_accents(alvo)
        ngrams = list(everygrams(texto_ocr.split(), min_words, len(alvo.split()) + 2))
        texts = [remove_accents(" ".join(i)) for i in ngrams]
        sentencas = texts
        if validate:
            width = len(alvo)
            sentencas = {*[texto_ocr[i:i + width] for i in range(len(texto_ocr) - (width - 1))], *sentencas}
            sentencas = filter(lambda x: len(x) < len(alvo) + 2 and len(x) > len(alvo) - 2, sentencas)
        percents = [[sentenca, round(similar(alvo, sentenca) * 100, 2)] for sentenca in sentencas]
        return max(percents, key=lambda x: x[1])
    except Exception as e:
        # print(e);raise

        return ["NÃO ENCONTRADO", 0.0]


def find_and_parse(text):
    date = search_dates(text)
    return date if date is None else datetime.strftime(date[0][1], "%d/%m/%Y")


def polygon_width(poly):
    try:
        box = poly.minimum_rotated_rectangle
        x, y = box.exterior.coords.xy
        edge_length = (
            Point(min(x), min(y)).distance(Point(max(x), min(y))),
            Point(max(x), min(y)).distance(Point(max(x), max(y))))
        length = edge_length[0]
        width = edge_length[1]
        return width, length
    except Exception as e:
        # print(e);raise
        pass


def filter_nome(candidates):
    for i, c in enumerate(candidates):
        c = c.upper()
        if not any(i.isdigit() for i in c) and len(c.split()) > 2:
            if c.split()[0] == "DR" or c.split()[0] == "MR": " ".join(c.split()[1:])
            if c.find(".") != -1:
                candidates[i] = c[c.find(".") + 1:].strip()
            if c.find(",") != -1:
                candidates[i] = c[c.find(",") + 1:].strip()
        else:
            del candidates[i]
    if len(set(candidates)) == 2: return [" ".join(candidates)]
    return list(set(candidates))


def filter_filiacao(candidates):
    for i, c in enumerate(candidates):
        if not any(i.isdigit() for i in c) and len(c.split()) > 2:
            if c.find(".") != -1:
                candidates[i] = c[c.find(".") + 1:].strip()
        else:
            del candidates[i]
    if len(candidates) > 1:
        return candidates[:2]
    return list(set(candidates))


def filter_numero(candidates):
    return list(set(filter(lambda x: sum(c.isdigit() for c in x) > 1, map(clean_punct, candidates))))


def filter_data(candidates):
    return list(set(filter(lambda x: x is not None, map(find_and_parse, candidates))))


def filter_cpf(candidates):
    return list(set(map(cpf.mask, filter(lambda x: cpf.validate(x), filter_numero(candidates)))))


def filter_sangue(candidates):
    casos = ['A', 'B', 'AB', 'O', 'A+', ' B+', 'O+', 'AB+', 'A-', 'B-', 'O- ', 'AB-']
    for i, c in enumerate(candidates):
        if c not in casos:
            del candidates[i]
    return list(set(candidates))


def filter_fator(candidates):
    casos = ['+', '-', 'POSITIVO', 'NEGATIVO', 'POS', 'NEG']
    for i, c in enumerate(candidates):
        if c not in casos:
            del candidates[i]
    return list(set(candidates))


def clean_punct(text):
    remove_puncts = str.maketrans('', '', '''!"#$%&'()*+,.:;<=>?@[\\]^_`{|}~''')
    text = text.translate(remove_puncts)
    return text


def find_label(labels, tipo, boxes2):
    candidates = []
    teste = []
    for label in labels:
        for box in boxes2:
            if match(boxes2[box]['box_full_text'], label)[1] > 90:
                found_box = Polygon(boxes2[box]['bounding_box_block'])
                for index in boxes2.copy():
                    distance = found_box.distance(Polygon(boxes2[index]['bounding_box_block']))
                    y_alvo = np.array(boxes2[box]['bounding_box_block'])[:, 1].max()
                    y_candidato = np.array(boxes2[index]['bounding_box_block'])[:, 1].min()
                    teste.append([distance, y_alvo, y_candidato, boxes2[index]['box_full_text']])
                for distance, y_alvo, y_candidato, texto in sorted(teste)[:3]:
                    if y_alvo < y_candidato and texto != boxes2[box]['box_full_text']:
                        st = texto.replace(label, "")
                        for i in sum(campos[:, 0], []):
                            if st.find(i) != -1: st = ''
                        if st != '': candidates.append(remove_accents(st).strip())

    if tipo == "NUMERO":
        return {labels[0]: filter_numero(candidates)}
    elif tipo == "DATA":
        return {labels[0]: filter_data(candidates)}
    elif tipo == "FILIACAO":
        return {labels[0]: filter_filiacao(candidates)}
    elif tipo == "CPF":
        return {labels[0]: filter_cpf(candidates)}
    elif tipo == "NOME":
        return {labels[0]: filter_nome(candidates)}
    elif tipo == "SANGUE":
        return {labels[0]: filter_sangue(candidates)}
    elif tipo == "FATOR":
        return {labels[0]: filter_fator(candidates)}
    else:
        return {labels[0]: list(set(candidates))}


def get_label(labels, tipo, boxes2):
    candidates = []
    for label in labels:
        for i in boxes2:
            bound = boxes2[i]['bounding_box_block']
            max_y, min_y, min_x = int(max([i[1] for i in bound])), int(min([i[1] for i in bound])), int(
                min([i[0] for i in bound]))
            ponto = Point(min_x, (np.mean([min_y, max_y], dtype=np.float16)))
            sorted_words = []
            for word in boxes2[i]['words']:
                polygon = Polygon(word['bounding_box'])
                sorted_words.append((ponto.distance(polygon),
                                     word['description'],
                                     word['confidence'],
                                     polygon))

            sorted_words = np.array(
                [(round(s[0], 2), remove_accents(s[1]), s[2], s[3]) for s in sorted(sorted_words, key=lambda x: x[0])])
            mat = match(' '.join(sorted_words[:, 1]).upper(), label)
            if mat[1] > 90:
                frase = ' '.join(sorted_words[:, 1]).partition(mat[0])[2]
                for camp in sum(campos[:, 0], []):
                    frase = frase.partition(camp)[0] if frase.find(camp) != -1 else frase
                candidates.append(frase.strip())
    if tipo == "NUMERO":
        return {labels[0]: filter_numero(candidates)}
    elif tipo == "DATA":
        return {labels[0]: filter_data(candidates)}
    elif tipo == "FILIACAO":
        return {labels[0]: filter_filiacao(candidates)}
    elif tipo == "CPF":
        return {labels[0]: filter_cpf(candidates)}
    elif tipo == "NOME":
        return {labels[0]: filter_nome(candidates)}
    elif tipo == "SANGUE":
        return {labels[0]: filter_sangue(candidates)}
    elif tipo == "FATOR":
        return {labels[0]: filter_fator(candidates)}
    return {labels[0]: list(set(candidates))}


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize("NFKD", input_str.upper().replace("\n", " "))
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])


campos = np.array([[["NOME"], "NOME"],
                   [["FILIAÇÃO"], "FILIACAO"],
                   [["CATEGORIA PROFISSIONAL", "PROFISSÃO"], ""],
                   [["DATA DE NASCIMENTO"], "DATA"],
                   [["DATA DE CONCLUSÃO"], "DATA"],
                   [["DATA DE EXPEDIÇÃO", "EXPEDIÇÃO"], "DATA"],
                   [["DATA DA DIPLOMAÇÃO"], "DATA"],
                   [["CRF"], "NUMERO"],
                   [["DIPLOMADO PELA", "DIPLOMA"], ""],
                   [["NATURALIDADE"], ""],
                   [["UNIDADE DE SAÚDE"], ""],
                   [["NACIONALIDADE"], "UF"],
                   [["EMISSÃO"], "DATA"],
                   [["GRUPO SANGUÍNEO"], "SANGUE"],
                   [["FATOR RH"], "FATOR"],
                   [["RG"], "NUMERO"],
                   [["TÍTULO DE ELEITOR", "TÍTULO"], "NUMERO"],
                   [["ZONA"], "NUMERO"],
                   [["SEÇÃO"], "NUMERO"],
                   [["LOCAL"], ""],
                   [["CPF"], "CPF"],
                   [["NUMERO"], "NUMERO"],
                   [["SERIE"], "NUMERO"],
                   [["AGENCIA", "AG"], "NUMERO"],
                   [["CONTA CORRENTE", "CORRENTE", "CONTA", "C/C", "C / C"], "NUMERO"]], dtype='object')
