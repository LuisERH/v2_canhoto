from gspread_pandas import Spread, Client
from json import load
from utils import text_extraction as tex
from pandas import Series, DataFrame
import os


def series_to_entities(series):
    entities = []
    if 'SERIE' in series.keys() and len(series['SERIE']) < 5:
        entities.append(find_stops(series['COMPLETE_TEXT'], series['SERIE'], 'SERIE'))

    if 'NOTA_FISCAL' in series.keys() and len(series['NOTA_FISCAL']) > 2 and len(series['NOTA_FISCAL']) < 14:
        entities.append(find_stops(series['COMPLETE_TEXT'], series['NOTA_FISCAL'], 'NOTA_FISCAL'))
    return {'entities': entities}


def find_stops(complete_text, label, key):
    start = complete_text.index(f' {label} ') + 1
    stop = start + len(label)
    return [start, stop, key]


def output_to_spread(output, file_bucket):
    output = {k: v for k, v in output.items() if k in ['COMPLETE_TEXT', 'NOTA_FISCAL', 'SERIE']}
    spread = Series(output)
    spread['COMPLETE_TEXT'] = tex.clean_text(spread['COMPLETE_TEXT'])
    spread['FEATURES'] = series_to_entities(spread)
    spread = DataFrame(spread).transpose()
    spread['NOTA_FISCAL'] = spread['NOTA_FISCAL'].apply(lambda x: f'"{x}"')
    spread = spread.join(DataFrame({'FILE': file_bucket}, index=[0]))
    try:
        spread['SERIE'] = spread['SERIE'].apply(lambda x: f'"{x}"' if x != None else x)
    except Exception as e:
        # print(e);raise
        spread['SERIE'] = None
    return spread[['COMPLETE_TEXT', 'FEATURES', 'NOTA_FISCAL', 'SERIE', 'FILE']]


def insert_on_spread(output, file_bucket):
    path = r"./credentials/Google.json"
    with open(path) as json_file: google = load(json_file)
    client = Client(config=google)
    sheet_obj = Spread('ner_canhoto', client=client)
    rows_count = sheet_obj.sheet.row_count
    headers = False if sheet_obj.sheet.row_count > 1 else True
    sheet_obj.df_to_sheet(output_to_spread(output, file_bucket), index=False, sheet='ner_canhoto',
                          headers=headers,
                          start=(rows_count if headers else rows_count + 1, 1))
