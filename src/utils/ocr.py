# from google.cloud.vision import types,ImageAnnotatorClient
from google.cloud.vision_v1p4beta1 import types, ImageAnnotatorClient
import cv2
import numpy as np
from utils import processing as pro

MIN_QUALIDADE_OCR = 68

ERRO_BAIXA_QUALIDADE = {"ERROR": "IMAGEM POSSUI BAIXA QUALIDADE"}
ERRO_LEITURA = {"ERROR": "FALHA AO FAZER LEITURA"}
ERRO_CONTEUDO_CANHOTO = {"ERROR": "IMAGEM DEVE CONTER APENAS O CONHOTO"}
ERRO_CANHOTO_PARCIAL = {"ERROR": "LEITURA PARCIAL DO CANHOTO"}


def detect_object(net, image):
    frame = pro.resize(image, 1500)
    classes, confidences, boxes = net.detect(frame, confThreshold=0.85, nmsThreshold=0.3)
    classId, confidence, box = max(zip(classes.flatten(), confidences.flatten(), boxes), key=lambda k: k[1])
    x, y, w, h = box
    return frame[y:y + h, x:x + w]


def detect_document_text(image):
    """ Aplicação da API do Google Vision OCR na IMagem
    Args:
        image (np.array): Imagem.
        size (int): Comprimento da Imagem.
    Returns:
        image (np.array): Imagem redimensionada.

    """
    client = ImageAnnotatorClient()
    image = types.Image(content=cv2.imencode('.png', image)[1].tostring())
    image_context = types.ImageContext(language_hints=["pt"])

    response = client.document_text_detection(
        image=image, timeout=1000, image_context=image_context
    )
    document = response.full_text_annotation
    return document


def align_image(image_ori, COMPRIMENTO_PADRAO_IMAGEM, net=False, check=True):
    if check:
        proportion, orientation = pro.check_proportion(image_ori.shape)

        if not proportion:  # imagem fora a proporçãos
            try:
                cropped = pro.rotate(detect_object(net, image_ori))
            except:
                cropped = pro.crop_img(image_ori)
            proportion_crop, orientation_crop = pro.check_proportion(cropped.shape)
            if proportion_crop:
                if orientation_crop == "VERTICAL":  # recorte ok + vertical
                    cropped = cv2.rotate(cropped, cv2.ROTATE_90_COUNTERCLOCKWISE)
            else:  # recorte fora a proporção
                image_ori = pro.resize(image_ori, COMPRIMENTO_PADRAO_IMAGEM)
                response = detect_document_text(image_ori)
                if len(response.text) < 30: return ERRO_LEITURA, 400
                poligonos = pro.find_polygons(response)
                if round(np.array(poligonos)[:, 2].mean() * 100, 2) < MIN_QUALIDADE_OCR:
                    return ERRO_BAIXA_QUALIDADE, 400
                x_min, y_min, x_max, y_max = pro.draw_box_around_text(image_ori, poligonos)
                crop_x_min = x_min - 30 if x_min - 30 >= 0 else 5
                crop_y_min = y_min - 30 if y_min - 30 >= 0 else 5
                cropped = image_ori[crop_y_min:y_min + (y_max - y_min) + 30, crop_x_min:x_min + (x_max - x_min) + 30]
                if max([cropped.shape[0], cropped.shape[1]]) / min([cropped.shape[0], cropped.shape[1]]) < 4.5:
                    return ERRO_LEITURA, 400
                if cropped.shape[0] / cropped.shape[1] > 0.25:
                    cropped = cv2.rotate(cropped, cv2.ROTATE_90_COUNTERCLOCKWISE)
        else:
            if orientation == 'VERTICAL':
                image_ori = cv2.rotate(image_ori, cv2.ROTATE_90_COUNTERCLOCKWISE)
            cropped = pro.resize(image_ori, COMPRIMENTO_PADRAO_IMAGEM)
    else:
        cropped = image_ori

    cropped = pro.resize(cropped, COMPRIMENTO_PADRAO_IMAGEM)
    response = detect_document_text(cropped)
    if len(response.text) < 10:
        return ERRO_LEITURA, 400
    if pro.is_upside_down(response):
        cropped = cv2.rotate(cropped, cv2.ROTATE_180)
        response = detect_document_text(cropped)
    return pro.resize(cropped, COMPRIMENTO_PADRAO_IMAGEM), response
