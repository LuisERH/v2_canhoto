from PIL import Image, ImageDraw
import cv2
import numpy as np
import matplotlib.pyplot as plt


def polygons_image(image, polygons, color=(0, 0, 255)):
    """Faz a plotagem dos retângulos em volta de cada palavra lida"""
    image = Image.fromarray(image)
    image = image.convert("RGB")
    draw = ImageDraw.Draw(image)
    for polygon, _, _ in polygons:
        draw.polygon(polygon.exterior.coords[:4], None, color)
        draw.text(polygon.centroid.coords, ".", fill="blue", align="center")
    image = np.array(image)
    return image


def draw_boxes(contours, image):
    for i, contour in enumerate(contours):
        x, y, w, h = cv2.boundingRect(contour)
        M = cv2.moments(contour)
        cx, cy = int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"])
        cv2.rectangle(image, ((x + w) - 30, y), (x + w, y + 20), (255, 255, 255), -1)
        cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 255), 1)
        cv2.putText(
            image, "#{}".format(str(i + 1).zfill(2)),
            ((x + w) - 33, y + 15), cv2.FONT_HERSHEY_PLAIN,
            1,
            (0, 0, 255),
            2,
        )
    return image


def compare_images(*image_list, next=False):
    image_list = list(image_list)
    if len(image_list) == 1 and type(image_list[0]) == list:
        image_list = np.squeeze(image_list)
    if next:
        for idx, image in enumerate(image_list, 1):
            if type(image) in [np.str_, str]: image = cv2.imread(image)
            plt.subplot(int(len(image_list) / 2) + 1, 2, idx)
            plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            plt.subplots_adjust(wspace=0.05, hspace=0.05)
            plt.axis("off")
    else:
        for image in image_list:
            if type(image) in [np.str_, str]: image = cv2.imread(image)
            plt.figure()
            plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            plt.axis("off")
    plt.show()
