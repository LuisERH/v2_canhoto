from werkzeug.datastructures import FileStorage
from flask_restplus import reqparse

busca = reqparse.RequestParser()
# busca = server.api.parser()
busca.add_argument('image',
                   location='files',
                   # required=True,
                   help='File - Imagem fotografada ou escaneada de um documento',
                   type=FileStorage)

busca.add_argument('image_url',
                   type=str,
                   location='form',
                   # default="https://www.imagefile.jpeg",
                   help="String - URL que contém a imagem de um documento.", )

busca.add_argument('find',
                   type=str,
                   location='form',
                   default='["Recebemos de","data de"]',
                   help='Lista - Termos que desejados para encontrar na extração do texto.')

# busca.add_argument('return_img',
#                    type=str,
#                    location='form',
#                    default="true",
#                    help='Booleano - Retorna a imagem em BASE64.')
