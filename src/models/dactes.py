from werkzeug.datastructures import FileStorage
from flask_restplus import reqparse

dacte = reqparse.RequestParser()
# dacte = server.api.parser()
dacte.add_argument('image',
                   location='files',
                   required=True,
                   help='File - Imagem fotografada ou escaneada de um dacte.',
                   type=FileStorage)

dacte.add_argument('find_pair',
                   type=str,
                   location='form',
                   default='["Nota Fiscal","No Protocolo"]',
                   help='Lista - Chaves de pares de informações desejados')

dacte.add_argument('image_url',
                   type=str,
                   location='form',
                   # default="https://www.imagefile.jpeg",
                   help="String - URL que contém a imagem.", )

dacte.add_argument('return_img',
                   type=str,
                   location='form',
                   default="true",
                   help='Booleano - Retorna a imagem em BASE64.')

dacte.add_argument('ancoras',
                   type=str,
                   location='form',
                   default='''{
                   "inferior_direito": ["VALOR TOTAL DO SERVIÇO", "VALOR A RECEBER"],
                   "inferior_esquero": ["RNTRC DA EMPRESA", "OBSERVAÇÕES"],
                   "superior_direito": ["MODAL", "NO PROTOCOLO"],
                   "superior_esquero": ["CFOP. NATUREZA DA PRESTAÇÃO", "ORIGEM DA PRESTAÇÃO"]
                   }''',
                   help="JSON - Campos especificados pelo usuário para checar se o documento está completo")
