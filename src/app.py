from controllers.canhotos import *
from controllers.dactes import *
from controllers.buscas import *
from controllers.documentos import *

app.config['UPLOAD_FOLDER'] = os.path.basename('./files')
app.config['JSON_AS_ASCII'] = False

server.run()
