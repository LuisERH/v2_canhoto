FROM python:3.8

WORKDIR /usr/app

COPY requirements.txt . 

RUN apt-get update
RUN python -m pip install --upgrade pip
RUN apt-get install -y libgl1-mesa-dev
RUN pip install -r requirements.txt
RUN pip install --upgrade google-cloud-vision
RUN pip uninstall opencv-contrib-python opencv-python -y
RUN pip install opencv-contrib-python==4.4.0.40
RUN apt-get install -y libzbar0 
RUN pip install pyzbar 
COPY . .

ENTRYPOINT ["python"]

WORKDIR /usr/app/src

CMD ["app.py"]